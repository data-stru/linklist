class LinkList {
    private Link first; // ref to the first link on the list
    private Link last; // ref to the last link on list

    public LinkList() // constructor
    {
        first = null; // no items on the list yet
    }

    public boolean isEmpty() // true if the list is empty
    {
        return (first == null);
    }

    // Insert at the start of the list
    public void insertFirst(int id, double dd) {
        // Make a new link
        Link newLink = new Link(id, dd);
        newLink.next = first; // newLink --> old first
        first = newLink; // first --> newLink
    }

    // Delete the first item (assumes the list is not empty)
    public Link deleteFirst() {
        Link temp = first; // save a reference to the link
        first = first.next; // delete it: first --> old next
        return temp; // return the deleted link
    }

    // Insert at the last of the list
    public void insertLast(int id, double dd) {
        // Make a new link
        Link newLink = new Link(id, dd);
         if (isEmpty()) {
            first = newLink; 
        } else {
            Link now = first;
            while (now.next != null) {
                now = now.next;
            }
            now.next = newLink; 
        }
    }

    // Delete the last item (assumes the list is not empty)
    public Link deleteLast() {
        if (first == null) {
            return null;
        } else if (first == last) {
            Link temp = last;
            first = last = null;
            return temp;
        } else {
            Link now = first;
            while (now.next != last) {
                now = now.next;
            }
            now.next = null;
            last = now;
            return last;
        }
    }
    

    public void displayList() {
        System.out.print("List (first-->last): ");
        Link current = first; // start at the beginning of the list
        while (current != null) // until the end of the list
        {
            current.displayLink(); // print data
            current = current.next; // move to the next link
        }
        System.out.println();
    }

    public void displayListLast() {
        System.out.print("List (last --> frist): ");
        Link current = first; 
        while (current != null) 
        {
            current.displayLink(); 
            current = current.next; 
        }
        System.out.println();
    }
}

