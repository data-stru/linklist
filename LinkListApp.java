class LinkListApp {
    public static void main(String[] args) {
        LinkList theList = new LinkList(); // make a new list
        theList.insertFirst(22, 2.99); // insert four items
        theList.insertFirst(44, 4.99);
        theList.insertFirst(66, 6.99);
        theList.insertFirst(88, 8.99);
        theList.displayList(); // display the list
        
        while (!theList.isEmpty()) { // until it's empty,
            Link aLink = theList.deleteFirst(); // delete a link
            System.out.print("Deleted "); // display it
            aLink.displayLink();
            System.out.println("");
        }
        
        theList.displayList(); // display the list


        //last --> Frist
        theList.insertLast(22, 2.99); // insert four items
        theList.insertLast(44, 4.99);
        theList.insertLast(66, 6.99);
        theList.insertLast(88, 8.99);
        theList.displayListLast(); // display the list

        while (!theList.isEmpty()) { // until it's empty,
            Link aLink = theList.deleteLast(); // delete a link
            System.out.print("Deleted "); // 

            aLink.displayLink();
            System.out.println("");
        }
        theList.displayListLast();


    } // end main()


} // end class LinkListApp
